//WAP to find the sum of two fractions.
#include<stdio.h>
struct addfract
{
	int num;
	int deno;
};
typedef struct addfract fract;
fract read_input()
{
	fract f;
	printf("Enter Numerator\n");
	scanf("%d",&f.num);
	printf("Enter denominator\n");
	scanf("%d",&f.deno);
	return f;
}
void find_num(fract f1,fract f2)
{
	int n,i,gcd,lcm,nu1,nu2,j,gcdf,n1,d1,n2,d2;
	if(f1.deno==f2.deno)
	{
      n=f1.num+f2.num;
     printf("The sum of %d/%d+%d/%d=%d/%d",f1.num,f1.deno,f2.num,f2.deno,n,f1.deno);
}
else
{
	for(i=1;i<=f1.deno&&i<=f2.deno;i++)
	{
   if(f1.deno%i==0&&f2.deno%i==0)
  {
  	gcd=i;
   }
}
lcm=(f1.deno*f2.deno)/gcd;
nu1=(lcm/f1.deno)*f1.num;
nu2=(lcm/f2.deno)*f2.num;
n=nu1+nu2;

for(j=1;j<=n&&j<=lcm;j++)
{
	if(n%j==0&&lcm%j==0)
	{
	gcdf=j;
}
}
n=n/gcdf;
lcm=lcm/gcdf;
printf("The sum of %d/%d+%d/%d=%d/%d\n",f1.num,f1.deno,f2.num,f2.deno,n,lcm);
}

int main()
{
	fract f1,f2;
	f1=read_input();
	f2=read_input();
	find_num(f1,f2);
	return 0;
}

}
