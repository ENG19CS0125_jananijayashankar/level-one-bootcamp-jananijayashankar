//Write a program to find the sum of n different numbers using 4 functions
#include <stdio.h>

int input_num()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

void input_array(int n, int arr[n])
{
    for(int i=0;i<n;i++){
        printf("Enter the element no %d of the array\n",i);
        scanf("%d",&arr[i]);
    }
}

int find_sum(int n, int arr[n])
{
    int sum=0;
    for(int i=0;i<n;i++) {
        sum += arr[i];
    }
    return sum;
}

int display_output(int n, int arr[n], int sum)
{
    int i;
    printf("The sum of"); 
    for(i=0;i<n-1;i++) {
        printf("%d+",arr[i]);
    }
    printf("%d=%d",arr[i],sum);
}

int main()
{
    int n,sum;
    n = input_num();
    int arr[n];
    input_array(n,arr);
    sum=find_sum(n,arr);
    display_output(n,arr,sum);
}


