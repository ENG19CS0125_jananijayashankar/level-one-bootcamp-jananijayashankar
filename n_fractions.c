//WAP to find the sum of n fractions.
#include<stdio.h>
struct fraction
{
	int num;
	int deno;
};
typedef struct fraction fract;
fract input_fraction()
{
	fract a;
	printf("Enter the numerator \n");
	scanf("%d",&a.num);
	printf("Enter the denominator \n");
	scanf("%d",&a.deno);
	return a;
}
int find_gcd(int n,int m)
{
	int gcdf;
	for(int j=1;j<=n&&j<=m;j++)
	{
	if(n%j==0&&m%j==0)
	{
	gcdf=j;
}
}
return gcdf;
}
fract compute_sum(fract a,fract b)
{
	fract res;
	if(a.deno==b.deno)
	{
	res.deno=a.deno;
	res.num=a.num+b.num;
}
else
{
	res.deno=a.deno*b.deno;
	res.num=(a.num*b.deno)+(b.num*a.deno);
}
return res;
}
int display_sum(int n,fract f,fract a[n])
{
	int d,b,gcd;
	for(int i=0;i<n;i++)
	{
	f=compute_sum(f,a[i]);
}
d=f.num;
b=f.deno;
gcd=find_gcd(d,b);
printf("The sum of the %d fractions entered is %d/%d",n, f.num/gcd, f.deno/gcd);
}
int main()
{
	int n;
	printf("Enter the number of fractions that needs to be added \n");
	scanf("%d",&n);
	fract f,a[n];
	f.num=0;
	f.deno=1;
	for(int i=0;i<n;i++)
	{
	printf("Enter for fraction %d \n",(i+1));
	a[i]=input_fraction();
}
display_sum(n,f,a);
return 0;
}

