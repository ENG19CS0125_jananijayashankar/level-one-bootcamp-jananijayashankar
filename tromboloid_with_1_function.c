//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>
int main()
{
    float h,d,b,vol;
    
    printf("Enter h:");
    scanf("%f",&h);
    
    printf("\nEnter d:");
    scanf("%f",&d);
    
    printf("\nEnter b:");
    scanf("%f",&b);
    
    vol=((h*d*b)+(d/b))/3;
    
    printf("Volume of tromboloid with h=%f,d=%f and b=%f is %f",h,d,b,vol);
    
    return 0;
}
