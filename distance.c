//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float read_coordinate()
{
	float x;
	printf("Enter the Coordinate value:");
	scanf("%f",&x);
	return x;
}

float find_distance(float x1,float y1,float x2,float y2)
{
	float d;
	d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
	return d;
}
void output(float x1,float y1,float x2,float y2,float dist)
{
	printf("Distance between the points(%f,%f) and (%f,%f) is %f ",x1,y1,x2,y2,dist);
}

int main()
{
	float x1,y1,x2,y2,dist;
	x1=read_coordinate();
	y1=read_coordinate();
	x2=read_coordinate();
	y2=read_coordinate();
	dist=find_distance(x1,y1,x2,y2);
	output(x1,y1,x2,y2,dist);
	
	return 0;
	
}
